# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin_user = User.new(username: 'Admin User', email: 'admin@example.com', password: 'admin123', admin: :true)
admin_user.build_address(
  address_line: 'Address Line',
  street: '7 Street',
  landmark: 'Near XYZ',
  city: 'Mumbai',
  state: 'Maharashtra',
  pin_code: '123456',
  phone_number: '7412589398'
)
admin_user.save