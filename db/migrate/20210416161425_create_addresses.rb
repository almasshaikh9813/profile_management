class CreateAddresses < ActiveRecord::Migration[6.1]
  def change
    create_table :addresses do |t|
      t.string :address_line
      t.string :street
      t.string :landmark
      t.string :city
      t.string :state
      t.string :pin_code
      t.string :phone_number
      t.integer :user_id

      t.timestamps
    end
  end
end
