class SessionsController < ApplicationController

  def new
  end

  # Creates a new session for user by authenticating the password
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      create_session(user)
    else
      flash.now[:danger] = 'There was something wrong with your login information.'
      render 'new'
    end
  end

  # Destroys the user session.
  def destroy
    session[:user_id] = nil
    flash[:success] = 'You have logged out'
    redirect_to root_path
  end

  private

  # Assigns user id as session identifier and redirects with success message.
  #
  # @param user [Object<User>]
  #
  # @return [void]
  def create_session(user)
    session[:user_id] = user.id
    flash[:success] = 'You have successfully logged in'
    redirect_to user_path(user)
  end

end
