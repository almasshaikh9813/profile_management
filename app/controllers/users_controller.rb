class UsersController < ApplicationController

  before_action :set_user, only: [:edit, :update, :show]
  before_action :require_same_user, only: [:edit, :update]
  before_action :require_user, only: [:edit, :update, :index, :show]

  # Renders new form for <tt>User</tt> registration
  def new
    @user = User.new
  end

  # Renders list of non admin users.
  def index
    @users = User.non_admin_users.paginate(page: params[:page], per_page: 5)
  end

  # Creates <tt>User</tt> with parameters received.
  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      flash[:success] = "User #{@user.username} registered successfully!"
      redirect_to user_path(@user)
    else
      render 'new'
    end
  end

  def show; end

  def edit; end

  # Updates <tt>User</tt> and redirects with success message.
  def update
    if @user.update(user_params)
      flash[:success] = 'Profile is successfully updated'
      redirect_to user_path(@user)
    else
      render 'edit'
    end
  end

  private

  # Sets the <tt>User</tt> of id received in parameter
  #
  # @return [Object<User>]
  def set_user
    @user = User.find(params[:id])
  end

  # Validates if the current user is the same user whose details are being updated or is an admin.
  #
  # @return [void]
  def require_same_user
    if current_user != @user && !current_user.admin?
      flash[:danger] = 'You can only edit your own details'
      redirect_to root_path
    end
  end

  # Permitted params for <tt>User</tt>
  #
  # @return [Object<ActionController::Parameters>]
  def user_params
    params.require(:user).permit(
      :username,
      :email,
      :password,
      :avatar,
      address_attributes: [
        :address_line,
        :street,
        :landmark,
        :city,
        :state,
        :pin_code,
        :phone_number
      ]
    )
  end

end