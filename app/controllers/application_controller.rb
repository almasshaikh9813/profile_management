class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user, :logged_in?, :require_user

  # Returns the current user logged in.
  #
  # @return [Object<User>]
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  # Verifies if <tt>User</tt> is logged in.
  def logged_in?
    !!current_user
  end

  # Displays error message if the <tt>User</tt> is not logged in.
  #
  # @return [void]
  def require_user
    if !logged_in?
      flash[:danger] = 'You must be logged in to perform that action'
      redirect_to root_path
    end
  end

end
