class Address < ApplicationRecord

  belongs_to :user

  validates :address_line,
    :street,
    :landmark,
    :city,
    :state,
    presence: true,
    length: { minimum: 3, maximum: 25 }
  validates :pin_code, :phone_number, presence: true
  VALID_PHONE_NUMBER_REGEX = /^(?:\+?\d{1,3}\s*-?)?\(?(?:\d{3})?\)?[- ]?\d{3}[- ]?\d{4}$/i.freeze
  validates :phone_number, format: { with: VALID_PHONE_NUMBER_REGEX, multiline: true }

end
