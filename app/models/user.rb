class User < ApplicationRecord

  has_one :address, inverse_of: :user, dependent: :destroy
  has_one_attached :avatar
  has_secure_password

  validates :username, presence: true, length: { minimum: 3, maximum: 25 }
  validates :email, presence: true, length: { maximum: 105 }
  validates :username, :email, uniqueness: { case_sensitive: false }
  VALID_EMAIL_REGEX = /([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})/i.freeze
  validates :email, format: { with: VALID_EMAIL_REGEX }

  scope :non_admin_users, -> { where(admin: :false) }

  accepts_nested_attributes_for :address

  before_save { self.email = email.downcase }

  # Verifies if <tt>User</tt> can edit the details.
  #
  # @param current_user [Object<User>]
  def can_edit?(current_user)
    !(self == current_user && current_user.admin?)
  end

end
