FactoryBot.define do
  factory :user do
    username { 'John' }
    email { 'john@example.com' }
    password { 'example' }
    admin { true }
  end

  factory :address do
    address_line { 'Address Line' }
    street { '7 Street' }
    landmark { 'Landmark' }
    city { 'Mumbai' }
    state { 'Maharashtra' }
    pin_code { 123456 }
    phone_number { 1234567890 }
    user { User.first }
  end

end
