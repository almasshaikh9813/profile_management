require 'rails_helper'

RSpec.describe Address, type: :model do

  context 'Validations' do

    subject { build(:address) }

    it { expect(subject).to belong_to(:user) }
    it { expect(subject).to validate_presence_of(:address_line) }
    it { expect(subject).to validate_presence_of(:street) }
    it { expect(subject).to validate_presence_of(:landmark) }
    it { expect(subject).to validate_presence_of(:city) }
    it { expect(subject).to validate_presence_of(:state) }
    it { expect(subject).to validate_presence_of(:pin_code) }
    it { expect(subject).to validate_presence_of(:phone_number) }
    it { expect(subject).to validate_length_of(:address_line).is_at_least(3) }
    it { expect(subject).to validate_length_of(:street).is_at_least(3) }
    it { expect(subject).to validate_length_of(:landmark).is_at_least(3) }
    it { expect(subject).to validate_length_of(:city).is_at_least(3) }
    it { expect(subject).to validate_length_of(:state).is_at_least(3) }
    it { expect(subject).to validate_length_of(:address_line).is_at_most(25) }
    it { expect(subject).to validate_length_of(:street).is_at_most(25) }
    it { expect(subject).to validate_length_of(:landmark).is_at_most(25) }
    it { expect(subject).to validate_length_of(:city).is_at_most(25) }
    it { expect(subject).to validate_length_of(:state).is_at_most(25) }

  end
end




