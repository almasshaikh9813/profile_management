require 'rails_helper'

RSpec.describe User, type: :model do

  context 'Validations' do

    subject { build(:user) }

    it { expect(subject).to have_one(:address) }
    it { expect(subject).to have_one_attached(:avatar) }
    it { expect(subject).to have_secure_password }
    it { expect(subject).to validate_presence_of(:username) }
    it { expect(subject).to validate_presence_of(:email) }
    it { expect(subject).to validate_length_of(:username).is_at_least(3) }
    it { expect(subject).to validate_length_of(:username).is_at_most(25) }
    it { expect(subject).to validate_length_of(:email).is_at_most(105) }
    it { expect(subject).to validate_uniqueness_of(:username).case_insensitive }
    it { expect(subject).to validate_uniqueness_of(:email).case_insensitive }

  end
end
